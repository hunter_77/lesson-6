function Baby(name, age, favoriteToy) {
    Person.call(this, name, age);
    this.favoriteToy = favoriteToy;
  }
  
  Baby.prototype = Object.create(Person.prototype);
  
  Baby.prototype.play = function() {
    console.log("this",this)
    return `${this.name} playing with ${this.favoriteToy}, ${this.favoriteToy} being the favorite toy.`
  }
  
  
  const baby = new Baby("Darsy", 3, "Barbie-girl");
  
  console.log(baby.play());